import unicodecsv


def read_csv(filename):
    with open(filename, 'rb') as f:
        reader = unicodecsv.DictReader(f)
        return list(reader)


enrollments = read_csv('enrollments.csv')

#####################################
#                 1                 #
#####################################

## Read in the data from daily_engagement.csv and project_submissions.csv
## and store the results in the below variables.
## Then look at the first row of each table.

daily_engagement = read_csv('daily-engagement.csv')
project_submissions = read_csv('project-submissions.csv')

print daily_engagement[0]
print enrollments[0]
print project_submissions[0]

from datetime import datetime as dt


# Takes a date as a string, and returns a Python datetime object.
# If there is no date given, returns None
def parse_date(date):
    if date == '':
        return None
    else:
        return dt.strptime(date, '%Y-%m-%d')


# Takes a string which is either an empty string or represents an integer,
# and returns an int or None.
def parse_maybe_int(i):
    if i == '':
        return None
    else:
        return int(i)


# Clean up the data types in the enrollments table
for enrollment in enrollments:
    enrollment['cancel_date'] = parse_date(enrollment['cancel_date'])
    enrollment['days_to_cancel'] = parse_maybe_int(enrollment['days_to_cancel'])
    enrollment['is_canceled'] = enrollment['is_canceled'] == 'True'
    enrollment['is_udacity'] = enrollment['is_udacity'] == 'True'
    enrollment['join_date'] = parse_date(enrollment['join_date'])

print enrollments[0]

# Clean up the data types in the engagement table
for engagement_record in daily_engagement:
    engagement_record['lessons_completed'] = int(
        float(engagement_record['lessons_completed']))
    engagement_record['num_courses_visited'] = int(
        float(engagement_record['num_courses_visited']))
    engagement_record['projects_completed'] = int(
        float(engagement_record['projects_completed']))
    engagement_record['total_minutes_visited'] = float(
        engagement_record['total_minutes_visited'])
    engagement_record['utc_date'] = parse_date(engagement_record['utc_date'])

print daily_engagement[0]

# Clean up the data types in the submissions table
for submission in project_submissions:
    submission['completion_date'] = parse_date(submission['completion_date'])
    submission['creation_date'] = parse_date(submission['creation_date'])

print project_submissions[0]

# Note when running the above cells that we are actively changing the contents of our data variables. If you try to run these cells multiple times in the same session, an error will occur.

## Investigating the Data

# uma lista
l = [1, 1, 1, 3, 4, 5]
# um set (conjunto)
s = set(l)
print 'tamanho de S: ' + str(len(s))

#####################################
#                 2                 #
#####################################

## Find the total number of rows and the number of unique students (account keys)
## in each table.

# print str(len(project_submissions)) + 'Project Submissions'
# print str(len(daily_engagement)) + ' Daily Engagement'
# print str(len(enrollments)) + ' Enrollments'

# unique_enrolled_students = set()
# for enrollment in enrollments:
#     unique_enrolled_students.add(enrollment['account_key'])
# print len(unique_enrolled_students)


for daily_engagement_value in daily_engagement:
    daily_engagement_value['account_key'] = daily_engagement_value['acct']
    del daily_engagement_value['acct']


def find_unique_values(iter, key):
    unique = set()
    for iterable in iter:
        # print iterable[key]
        # print iterable['account_key']
        # if 'is_udacity' in iterable and iterable['is_udacity']:
        #     del iterable
        # else:
        unique.add(iterable[key])
    return unique


print 'Valores unicos'
print str(len(find_unique_values(enrollments, 'account_key'))) + ' Unique Enrollments'
print str(len(find_unique_values(project_submissions, 'account_key'))) + ' Unique Project Submissions'
print str(len(find_unique_values(daily_engagement, 'account_key'))) + ' Unique  Daily Engagement'

print daily_engagement[0]['account_key']
unique_project_submitters = find_unique_values(project_submissions, 'account_key')
unique_enrolled_students = find_unique_values(enrollments, 'account_key')
unique_engagement_students = find_unique_values(daily_engagement, 'account_key')

print 'Investigando pq dos 1302 alunos, pq apenas 1237 tem engagamento'
# for enrollment in enrollments:
#     student = enrollment['account_key']
#     if student not in unique_engagement_students:
#         print enrollment
#         break

num_problem_students = 0
for enrollment in enrollments:
    student = enrollment['account_key']
    if (student not in unique_engagement_students and
            enrollment['join_date'] != enrollment['cancel_date']):
        print enrollment
        num_problem_students += 1

print num_problem_students

udacity_test_accounts = set()
for enrollment in enrollments:
    if 'is_udacity' in enrollment and enrollment['is_udacity']:
        udacity_test_accounts.add(enrollment['account_key'])

print 'udacity acc test: '
print str(len(udacity_test_accounts))


def remove_udacity_accounts(data):
    non_udacity_data = []
    for data_point in data:
        if data_point['account_key'] not in udacity_test_accounts:
            non_udacity_data.append(data_point)

    return non_udacity_data


print 'removendo accounts'
##unique_project_submitters = find_unique_values(project_submissions, 'account_key')
# unique_enrolled_students = find_unique_values(enrollments, 'account_key')
# unique_engagement_students = find_unique_values(daily_engagement, 'account_key')

non_udacity_enrollments = remove_udacity_accounts(enrollments)
non_udacity_engagement = remove_udacity_accounts(daily_engagement)
non_udacity_submissions = remove_udacity_accounts(project_submissions)

print len(non_udacity_enrollments)
print len(non_udacity_engagement)
print len(non_udacity_submissions)

# Removendo repetidos novamente.
print 'fazendo limpeza sem as contas udacity'

unique_project_submitters = find_unique_values(non_udacity_submissions, 'account_key')
unique_enrolled_students = find_unique_values(non_udacity_enrollments, 'account_key')
unique_engagement_students = find_unique_values(non_udacity_engagement, 'account_key')
print 'valores distintos'
print str(len(unique_project_submitters)) + ' Unique Project Submissions'
print str(len(unique_enrolled_students)) + ' Unique Enrollments'
print str(len(unique_engagement_students)) + ' Unique  Daily Engagement'